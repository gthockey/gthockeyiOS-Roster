# GTHockeyRoster

This package is a utility framework to allow for easy setup and maitenance of the roster tab in GT Hockey's iOS client applications.

## Installation
You can install `GTHockeyRoster` using Swift Package Manager using the following:
```
dependencies: [
    .package(url: "https://gitlab.com/gthockey/gthockey-iOSRoster.git", .upToNextMajor(from: "1.0.0"))
]
```

## Contributing
This package has its own example application to use and test its features on smaller scale. If you want to add to the package, make sure to test your work in the example application first -> `Example/GTHockeyRosterExample/`

Once you are confident in your additions and are ready to update the package follow these steps to publish your changes:
1. Navigate to `GTHockeyRoster` in command line and run the following two commands to unsure the package can build and passes all tests. You can proceed if both steps complete successfully.
```
$ swift build
$ swift test
```
2. Add, commit, and push your changes
```
$ git add .
$ git commit -m "[your commit message]"
$ git push
```
3. Tag your commit
```
$ git tag <new version>
```
4. Push your changes to that specific tag
```
$ git push origin <new version>
```
