//
//  RosterList.swift
//  
//
//  Created by Caleb Rudnicki on 2/5/22.
//

import SwiftUI
import GTHockeyUtils

public struct RosterList: View {
    
    @State private var roster: [Position: [Player]] = [:]
    
    private let endpoint: String
    
    public init(from endpoint: String) {
        self.endpoint = endpoint
    }
    
    public var body: some View {
        List {
            ForEach(Array(roster.keys.sorted()), id: \.self) { key in
                if let positionGroup = roster[key], !positionGroup.isEmpty {
                    SingleTableHeader(key.displayString)
                        .listRowBackground(Color.background)
                    ForEach(positionGroup) { player in
                        NavigationLink(destination: PlayerView(player: player)
                                        .background(Color.background)) {
                            if let number = player.number, key != .manager {
                                DoubleLabelTableCell(label1: "# \(number)",
                                                     label2: "\(player.firstName) \(player.lastName)")
                                    .tag(player.id)
                            } else {
                                SingleLabelTableCell(label: "\(player.firstName) \(player.lastName)")
                                    .tag(player.id)
                            }
                        }
                        .listRowBackground(Color.background)
                    }
                }
            }
        }
        .onAppear { fetchRoster() }
        .listStyle(.plain)
        .navigationTitle("Roster")
        .font(.heading3)
    }
    
    private func fetchRoster() {
        RosterManager.shared.getRoster(from: endpoint, completion: { result in
            switch result {
            case .success(let players):
                var forwards: [Player] = []
                var defense: [Player] = []
                var goalies: [Player] = []
                var managers: [Player] = []
                
                for player in players {
                    switch player.position {
                    case .forward:
                        forwards.append(player)
                    case .defense:
                        defense.append(player)
                    case .goalie:
                        goalies.append(player)
                    case .manager:
                        managers.append(player)
                    }
                }
                
                var tempRoster: [Position: [Player]] = [:]
                tempRoster[.forward] = forwards
                tempRoster[.defense] = defense
                tempRoster[.goalie] = goalies
                tempRoster[.manager] = managers
                self.roster = tempRoster
            case .failure(let error):
                // TODO: Present an error to the user
                print(error.localizedDescription)
            }
        })
    }
    
}

struct RosterList_Previews: PreviewProvider {
    static var previews: some View {
        RosterList(from: "https://www.gthockey.com/api/players/")
    }
}
