//
//  PlayerView.swift
//  
//
//  Created by Caleb Rudnicki on 2/5/22.
//

import SwiftUI
import SDWebImageSwiftUI
import GTHockeyUtils

internal struct PlayerView: View {
    
    internal var player: Player
    
    internal var body: some View {
        ScrollView(showsIndicators: false) {
            // Player Header
            HStack(spacing: 10) {
                WebImage(url: player.headshotURL)
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade)
                    .scaledToFit()
                    .cornerRadius(5)
                    .frame(width: 100, height: 100, alignment: .center)
                VStack(alignment: .leading) {
                    CustomText(player.firstName, font: .heading3)
                    CustomText(player.lastName, font: .heading3)
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top, 20)
            
            // Number and Position Nugget
            HStack(spacing: 20) {
                if let number = player.number {
                    BasicTextNugget(title: "Number", content: "\(number)")
                }
                BasicTextNugget(title: "Position", content: player.position.displayString)
            }
            .frame(maxWidth: .infinity)
            .padding(.top, 20)
                              
            // Hometown and Major Nugget
            HStack(spacing: 20) {
                BasicTextNugget(title: "Hometown", content: player.hometown)
                BasicTextNugget(title: "Major", content: player.school)
            }
            .frame(maxWidth: .infinity)
            .padding(.top, 30)
            
            if let bio = player.bio, !bio.isEmpty {
                LongTextNugget(title: "Bio", content: bio)
                    .padding(.top, 30)
            }
            
            VStack(alignment: .leading) {
                CustomText("Photos", font: .captionSemibold)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 5)
                    .padding(.leading, 10)
                WebImage(url: player.imageURL)
                    .resizable()
                    .indicator(.activity)
                    .transition(.fade)
                    .scaledToFit()
                    .cornerRadius(5)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding([.top, .bottom], 20)
                    .padding([.leading, .trailing], 10)
            }
            .frame(maxWidth: .infinity)
            .background(Color.secondaryBackground)
            .cornerRadius(5)
            .padding([.top, .bottom], 30)

        }
        .padding(.horizontal, 15)
        .background(Color.background.edgesIgnoringSafeArea(.all))
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct PlayerView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerView(player: RosterTestModels.player1)
    }
}
