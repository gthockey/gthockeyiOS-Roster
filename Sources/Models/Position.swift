//
//  Position.swift
//  
//
//  Created by Caleb Rudnicki on 2/5/22.
//

import Foundation

/// An enumeration to represent a possible position for each `Player` object
internal enum Position: String, Comparable, Decodable {
    
    case forward = "F"
    case defense = "D"
    case goalie = "G"
    case manager = "M"

    /// A full length string to display the position to a user
    var displayString: String {
        switch self {
        case .forward: return "Forward"
        case .defense: return "Defense"
        case .goalie: return "Goalie"
        case .manager: return "Manager"
        }
    }
    
    /// A ordering value to be used to show positions in a certain order
    var sortOrder: Int {
        switch self {
        case .forward: return 0
        case .defense: return 1
        case .goalie: return 2
        case .manager: return 3
        }
    }
    
    static func <(lhs: Position, rhs: Position) -> Bool {
        lhs.sortOrder < rhs.sortOrder
    }
    
}
