//
//  Player.swift
//  
//
//  Created by Caleb Rudnicki on 2/5/22.
//

import Foundation

/// An object to represent a member of the team
internal struct Player: Identifiable, Decodable {
    
    let id: Int
    let firstName: String
    let lastName: String
    let position: Position
    let number: Int?
    let hometown: String
    let school: String
    let imageURL: URL?
    let headshotURL: URL?
    let bio: String?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case imageURL = "image"
        case headshotURL = "headshot"
        case id, position, number, hometown, school, bio
    }
    
}
