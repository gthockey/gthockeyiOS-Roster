//
//  RosterTestModels.swift
//  
//
//  Created by Caleb Rudnicki on 2/9/22.
//

import Foundation

internal struct RosterTestModels {
    
    // MARK: Test Player Objects
    
    static let player1 = Player(id: 57,
                                firstName: "Garrett",
                                lastName: "Schueller",
                                position: .defense,
                                number: 14,
                                hometown: "Minneapolis, MN",
                                school: "Mechanical Engineering",
                                imageURL: URL(string: "https://prod.gthockey.com/media/players/SchuellerPic.jpg")!,
                                headshotURL: URL(string: "https://prod.gthockey.com/media/players/Gart.png")!,
                                bio: """
                                Garrett is a third-year defenseman who came to us from Southwest High School in Minnesota. Moving around with his family, Garrett found himself playing youth hockey in several different cities around the country. Starting at age 5, he played for the Austin Roadrunners, feeder team for the Dallas Stars, he claims. Following that and a move to Minneapolis, Garrett joined the Minneapolis Storm, a team that he says had the dustiest jerseys in the Twin City area. Entering his high school years, Garrett transitioned over to the Minneapolis High School’s hockey team who were known as the Junk Yard Dogs.
                                
                                Garrett’s favorite NHL team is the Minnesota Wild and his favorite NHL player is Derek Boogaard because of his goonish style of play that Garrett’s aspires towards.

                                After wearing #14 last season, Garrett now wears #4 for games. His only reasoning for picking that number is because it is very “neat”. In his first two seasons with the Yellow Jackets, Garrett has tallied 4 goals and 12 assists in 48 games played.
                                """
                                )

    static let player2 = Player(id: 83,
                                firstName: "Keely",
                                lastName: "Culbertson",
                                position: .manager,
                                number: nil,
                                hometown: "New Orleans, LA",
                                school: "Computer Science",
                                imageURL: URL(string: "https://prod.gthockey.com/media/players/Primary_Logo_384K4X4.png")!,
                                headshotURL: URL(string: "https://prod.gthockey.com/media/players/Primary_Logo_8diSQ1A.png")!,
                                bio: "")
    
}
