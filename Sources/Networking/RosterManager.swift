//
//  RosterManager.swift
//  
//
//  Created by Caleb Rudnicki on 2/6/22.
//

import Foundation
import GTHockeyUtils

/// The networking layer class for fetching a roster
internal class RosterManager {
    
    internal static var shared = RosterManager()
    
    /// Fetches roster of player with a given url endpoint
    /// - Parameters:
    ///   - urlString: a `String` that represents the endpoint to reach out to
    ///   - completion: a completion type of either `.success([Player])` or `.failure(NetworkingError)` indicating
    ///   the results of the fetch
    internal func getRoster(from urlString: String, completion: @escaping (Result<[Player], NetworkingError>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidUrl))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            do {
                let players = try JSONDecoder().decode([Player].self, from: data)
                completion(.success(players))
            } catch {
                completion(.failure(.parsingError))
            }
        }
        task.resume()
    }
    
}
