//
//  GTHockeyRosterExampleApp.swift
//  GTHockeyRosterExample
//
//  Created by Caleb Rudnicki on 2/15/22.
//

import SwiftUI
import GTHockeyRoster

@main
struct GTHockeyRosterExampleApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                RosterList(from: "https://www.gthockey.com/api/players/")
                    .background(Color.background)
            }
        }
    }
}
